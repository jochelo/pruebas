# pruebas

Pruebas realizadas con Spring Boot App + Rest + JPA + H2

Instrucciones de ejecucion

1. Clonar el proyecto
2. Actualizar dependencias
3. Ejecutar como Spring Boot App


Para la funcion MaxProfit <br />
Llamar el controlador rest http://localhost:8080/api/maxProfit con metodo GET pasando en el Body los precios de las acciones <br />
Ejemplo [44, 30, 22, 32, 35, 30, 41, 38, 15] <br />
No olvidar agregar la cabecera Content-Type = application/json<br />

Para la funcion acortar URL <br />
Llamar el controlador rest http://localhost:8080/api/shortenUrl con metodo POST pasando en el Body la url. <br />
Ejemplo http://www.facebook.com <br />
No olvidar agregar la cabecera Content-Type = text/plain <br />

Para la funcion Obtener url por id <br />
Llamar el controlador rest http://localhost:8080/api/shortenUrl con metodo GET pasando en el Body el codigo obtenido al acortar la url. <br />
Ejemplo 89405628 <br />
No olvidar agregar la cabecera Content-Type = text/plain <br />