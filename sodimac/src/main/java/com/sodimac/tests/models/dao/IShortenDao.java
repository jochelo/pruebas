package com.sodimac.tests.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.sodimac.tests.services.Tupla;

public interface IShortenDao extends CrudRepository<Tupla, String> {

}
