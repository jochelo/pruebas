package com.sodimac.tests.services;

public interface IShortenService {

	public Tupla decodeUrl(String code);
	
	public String codeUrl(String url);	

}
