package com.sodimac.tests.services;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tuplas")
public class Tupla implements Serializable{
	
	private static final long serialVersionUID = -3280846596901815760L;
	
	@Id
	private String key;
	private String value ;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
