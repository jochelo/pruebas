package com.sodimac.tests.services;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class ProfitServiceImpl implements IProfitService {

	@Override
	public Long getMaxProfit(List<Long> actions) {
		return maxProfit(actions);
	}

	public Long maxProfit(List<Long> prices) {

	    Long min=prices.get(0);
	    Long result=Long.valueOf(0);

	    // [44, 30, 22, 32, 35, 30, 41, 38, 15]
	    //  0 -  0 - 10 - 13 - 13 - 19 - 19 - 19
	    // 30 - 22 - 22 - 22 - 22 - 22 - 22 - 15

	    for(int i=1; i<prices.size(); i++){
	        result = Math.max(result, prices.get(i)-min);
	        min = Math.min(min, prices.get(i));
	    }
	    // 2 - 3 - 4 - 2
	    // 1 - 2 - 2
	    // 2 - 2 - 2
	    if (result.equals(Long.valueOf(0))) {
	    	return result = Long.valueOf(-1);
	    }

	    return result;
	}
}
