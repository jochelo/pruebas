package com.sodimac.tests.services;

import java.nio.charset.StandardCharsets;

import org.apache.commons.validator.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.hash.Hashing;
import com.sodimac.tests.models.dao.IShortenDao;

@Service
public class ShortenServiceImpl implements IShortenService {
	
	@Autowired
	IShortenDao shortenDao;

	@Override
	public Tupla decodeUrl(String code) {

		return shortenDao.findById(code).orElse(null);
		}

	@Override
	public String codeUrl(String url) {
		
		String[] schemes = { "http", "https" };

		UrlValidator urlValidator = new UrlValidator(schemes);
		if (urlValidator.isValid(url)) {
			String id = Hashing.murmur3_32().hashString(url, StandardCharsets.UTF_8).toString();
			Tupla tupla = new Tupla();
			tupla.setKey(id);
			tupla.setValue(url);
			shortenDao.save(tupla);
			return id;
		} else {
			return "-1";
		}
	}

}
