package com.sodimac.tests.services;

import java.util.List;

public interface IProfitService {

	public Long getMaxProfit( List<Long> actions );

}
