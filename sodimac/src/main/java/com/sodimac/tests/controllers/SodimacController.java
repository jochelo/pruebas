package com.sodimac.tests.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sodimac.tests.services.IProfitService;
import com.sodimac.tests.services.IShortenService;
import com.sodimac.tests.services.Tupla;

@RestController
@RequestMapping("/api")
public class SodimacController {

	@Autowired
	IProfitService profitService;

	@Autowired
	IShortenService shortenService;

	Map<String, Object> responseMap = null;

	@GetMapping("/maxProfit")
	public ResponseEntity<?> maxProfit(@RequestBody List<Long> actions) {

		Map<String, Object> responseMap = null;
		Long maxProfit = null;

		if (actions.isEmpty() || actions.size() < 2) {
			responseMap = new HashMap<>();
			responseMap.put("mensaje", "Debe contener mas de un elemento para evaluar");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}

		maxProfit = profitService.getMaxProfit(actions);

		return new ResponseEntity<Long>(maxProfit, HttpStatus.OK);
	}

	@GetMapping("/shortenUrl")
	public ResponseEntity<?> decodeUrl(@RequestBody String id) {

		Tupla tupla = null;

		try {
			tupla = shortenService.decodeUrl(id);
		} catch (DataAccessException e) {
			responseMap = new HashMap<>();
			responseMap.put("mensaje", "Error al hacer la consulta en base de datos");
			responseMap.put("error", e.getMessage().concat(" : ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (tupla.getValue() != null) {
			return new ResponseEntity<String>(tupla.getValue(), HttpStatus.OK);
		} else {
			responseMap = new HashMap<>();
			responseMap.put("mensaje", "No se encuentra url para la clave ingresada");
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/shortenUrl")
    public ResponseEntity<?> codeUrl(@RequestBody String id) {

    	String url = null;
    	try {
         url = shortenService.codeUrl(id);
	}catch (DataAccessException e) {
		responseMap = new HashMap<>();
		responseMap.put("mensaje", "Error al hacer la consulta en base de datos");
		responseMap.put("error", e.getMessage().concat(" : ").concat(e.getMostSpecificCause().getMessage()));
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
	}
        if (url.equalsIgnoreCase("-1")) {
        	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);        	
        } else {
            return new ResponseEntity<>("El codigo correspondiente a la url ingresada es: " + url, HttpStatus.OK);        	
        }
    }			
}